﻿using Cisco.Common.ImpactSupport.Configuration;
using Cisco.Common.ImpactSupport.Helpers;
using Cisco.Common.ImpactSupport.Services;
using System;

namespace RqlDataApiLibrary
{
    public class BatchRepository
    {
        private const string apiUser = "service.EmailGeneration";
        private const string apiPass = "xZWgLHa01ovo";

        protected IApiService ApiService { get; set; }

        public BatchRepository(string url)
        {
            var authSettings = new AuthenticationSetting {SiteUrl = url, Username = apiUser, Password = apiPass};
            ApiService = RqlApiService.GetInstance(authSettings,LoggingHelper.GetStaticLogger());
        }

        public string GetBatchById(string storeId)
        {
            if (string.IsNullOrWhiteSpace(storeId))
            {
                throw new ApplicationException(string.Format("Invalid store ID, '{0}.", storeId));
            }

            return ApiService.GetStore(storeId).ToString();
        }
    }
}
