#import System.Configuration
import sys
import xml.etree.ElementTree as ET
import clr
clr.AddReferenceToFileAndPath(r'c:\BitBucket\IronPythonTestAutomationPrototype\RqlDataApiLibrary\bin\Debug\RqlDataApiLibrary.dll')

from RqlDataApiLibrary import *

def LoadBatch(url, storeId):
    batchRepo = BatchRepository(url)
    return batchRepo.GetBatchById(storeId)

def main():
    testStoreId = 'str-a1bae93197fe45a9adfb34b098c69e54'
    url = 'cisco-cpai-dev.services-exchange.com'
    testDataPath = 'C:\BitBucket\IronPythonTestAutomationPrototype\IronPythonTestAutomationPrototype\TestData\\'

    # load store from c# lib which access API
    storeXml = LoadBatch(url, testStoreId)

    # write store to file
    filePath = testDataPath + 'batchStore.xml' 
    f = open(filePath, 'w')
    f.write(storeXml)
    f.close()
    
    root = ET.fromstring(storeXml)
    print 'Root node: ' + root.tag

    batchElement = root.find('./AQ_Batches')
    print 'Batch node: ' + batchElement.tag

    assert(batchElement.tag == 'AQ_Batches')

    compareRoot = ET.parse(filePath)

    # not working yet...
    #assert(compareRoot == root)

    raw_input('press \'Enter\' to exit')

if __name__ == "__main__":
    sys.exit(int(main() or 0))